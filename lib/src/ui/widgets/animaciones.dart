import 'package:flutter/material.dart';
import 'dart:math' as Math;
class AnimacionesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CuadradoAnimado(),
      ),
    );
  }
}

class CuadradoAnimado extends StatefulWidget {
  const CuadradoAnimado({
    Key key,
  }) : super(key: key);

  @override
  _CuadradoAnimadoState createState() => _CuadradoAnimadoState();
}

class _CuadradoAnimadoState extends State<CuadradoAnimado>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation<double> rotacion;
  Animation<double> opacidad;

  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 4000),
    );

    rotacion = Tween(begin: 0.0, end: 2 * Math.pi).animate(CurvedAnimation(
      curve: Curves.elasticInOut,
      parent: _animationController,
    ));

    opacidad = Tween(begin: 0.1, end: 1.0).animate(_animationController);

    _animationController.addListener(() {
      print('Status: ${_animationController.status}');
    });
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController,
      child: GestureDetector(
        child: _Rectangulo(),
        onDoubleTap: () {
          _animationController.reset();
        },
        onTap: () {
          rotacion.value == 0.0
              ? _animationController.forward()
              : _animationController.reverse();
        },
        onLongPress: () {
          Navigator.pop(context);
        },
      ),
      builder: (BuildContext context, Widget child) {
        print('rotacion: ' + rotacion.value.toString());
        return Transform.rotate(
          angle: rotacion.value,
          child: Opacity(
            opacity: opacidad.value,
            child: child,
          ),
        );
      },
    );
  }
}

class _Rectangulo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 70,
      height: 70,
      decoration: BoxDecoration(color: Colors.red),
    );
  }
}
