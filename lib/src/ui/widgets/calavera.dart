import 'package:flutter/material.dart';

class Calavera extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Image.asset(
      "assets/foals_calavera_black.png",
      height: 75,
    );
  }
}
