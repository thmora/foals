import 'package:foals/src/utils/styles.dart';
import 'package:flutter/material.dart';

class WavePaint extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    paint.color = colores.darkLetra;
    paint.style = PaintingStyle.fill;
    paint.strokeWidth = 25.0;
    final path = Path();
    path.moveTo(0, 0);
    path.lineTo(0, size.height * .25);
    path.quadraticBezierTo(size.width * .25, size.height * 0.35,
        size.width * .5, size.height * 0.25);
    path.quadraticBezierTo(
        size.width * .75, size.height * 0.15, size.width, size.height * 0.25);
    path.lineTo(size.width, 0);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class CurvePaint extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();

    paint.color = colores.darkLetra;
    paint.style = PaintingStyle.fill;
    paint.strokeWidth = 25.0;

    final path = Path();
    /* CURVADO */
    path.moveTo(0, 0);
    path.lineTo(0, size.height * .4);
    path.quadraticBezierTo(
        size.width * .2, size.height * 0.5, size.width, size.height * 0.25);
    path.lineTo(size.width, 0);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class PicoPaint extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();

    paint.color = colores.darkLetra;
    paint.style = PaintingStyle.fill;
    paint.strokeWidth = 25.0;

    final path = Path();
    /* PICO */
    path.moveTo(0, 0);
    path.lineTo(0, size.height * 0.2);
    path.lineTo(size.width * .5, size.height * .5);
    path.lineTo(size.width, size.height * 0.2);
    path.lineTo(size.width, 0);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
