import 'package:flutter/material.dart';

class RegisterButton extends StatelessWidget {
  final VoidCallback _onPressed;

  RegisterButton({Key key, VoidCallback onPressed})
      : _onPressed = onPressed,
        super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20.0),
      child: RaisedButton(
        padding: EdgeInsets.symmetric(horizontal: 70, vertical: 20),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        onPressed: _onPressed,
        color: Theme.of(context).primaryColor,
        child: Text(
          'Registrarse',
          style: TextStyle(color: Colors.white, fontSize: 22),
        ),
      ),
    );
  }
}
