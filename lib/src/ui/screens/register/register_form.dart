import 'package:foals/src/bloc/authentication_bloc/auth_bloc.dart';
import 'package:foals/src/bloc/register_bloc/register_bloc.dart';
import 'package:foals/src/ui/screens/register/register_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RegisterForm extends StatefulWidget {
  RegisterForm({Key key}) : super(key: key);
  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _direccionController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _apellidoController = TextEditingController();
  final TextEditingController _dniController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _confirmPasswordController =
      TextEditingController();

  RegisterBloc _registerBloc;

  bool get isPopulated =>
      _emailController.text.isNotEmpty &&
      _passwordController.text.isNotEmpty &&
      _direccionController.text.isNotEmpty &&
      _nameController.text.isNotEmpty &&
      _apellidoController.text.isNotEmpty &&
      _dniController.text.isNotEmpty &&
      _phoneController.text.isNotEmpty &&
      _confirmPasswordController.text.isNotEmpty;

  bool isRegisterButtonEnabled(RegisterState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    _registerBloc = BlocProvider.of<RegisterBloc>(context);
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
    _direccionController.addListener(_onDireccionChanged);
    _dniController.addListener(_onDNIChanged);
    _nameController.addListener(_onNameChanged);
    _apellidoController.addListener(_onApellidoChanged);
    _phoneController.addListener(_onPhoneChanged);
    _confirmPasswordController.addListener(_onConfirmPasswordChanged);
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _dniController.dispose();
    _direccionController.dispose();
    _nameController.dispose();
    _apellidoController.dispose();
    _phoneController.dispose();
    _confirmPasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterBloc, RegisterState>(
        listener: (context, state) {
      // Si estado es submitting

      if (state.isSubmitting) {
        Scaffold.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Registrandose'),
                  CircularProgressIndicator(),
                ],
              ),
            ),
          );
      }

      // Si estado es success

      if (state.isSuccess) {
        BlocProvider.of<AuthBloc>(context).add(LoggedIn());
        Navigator.of(context).pop();
      }

      // Si estado es failure

      if (state.isFailure) {
        Scaffold.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(SnackBar(
            content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Falló al registrarse'),
                Icon(Icons.error)
              ],
            ),
            backgroundColor: Colors.red,
          ));
      }
    }, child: BlocBuilder<RegisterBloc, RegisterState>(
      builder: (context, state) {
        return Padding(
          padding: EdgeInsets.all(20),
          child: Form(
            child: ListView(
              children: <Widget>[
                _campo(
                  label: 'Correo',
                  campoController: _emailController,
                  hint: 'Ej: email@email.com',
                  icono: Icons.mail_outline,
                  mensajeError: 'Correo Inválido',
                  validador: state.isEmailValid,
                  keyboardType: TextInputType.emailAddress,
                ),
                _campo(
                  label: 'DNI',
                  campoController: _dniController,
                  hint: 'Ej: 11222333',
                  icono: Icons.account_circle,
                  mensajeError: 'DNI no válido',
                  validador: state.isDNIValid,
                  keyboardType: TextInputType.number,
                ),
                _campo(
                  validador: state.isDireccionValid,
                  label: 'Dirección',
                  hint: 'Ej: Las Palmas 1023',
                  icono: Icons.home,
                  campoController: _direccionController,
                  mensajeError: 'Dirección no válida',
                ),
                _campo(
                  validador: state.isNameValid,
                  label: 'Nombre',
                  hint: 'Ej: Juan',
                  icono: Icons.person_outline,
                  campoController: _nameController,
                  mensajeError: 'Por favor, introduzca su nombre',
                ),
                _campo(
                  validador: state.isApellidoValid,
                  label: 'Apellido',
                  hint: 'Ej: Perez',
                  icono: Icons.person_outline,
                  campoController: _apellidoController,
                  mensajeError: 'Por favor, Introduzca su apellido',
                ),
                _campo(
                  validador: state.isPhoneValid,
                  label: 'Teléfono',
                  hint: 'Sin 0, 15 y espacios, ej: 111 5 222 333',
                  icono: Icons.phone,
                  campoController: _phoneController,
                  mensajeError: 'Por favor, compartanos su número telefónico',
                  keyboardType: TextInputType.phone,
                ),
                _campo(
                  validador: state.isPasswordValid,
                  label: 'Contraseña',
                  hint: 'Ej: aaaa1111',
                  icono: Icons.lock_outline,
                  campoController: _passwordController,
                  mensajeError: 'Contraseña no válida',
                  obscureText: true,
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: state.isConfirmPasswordValid ? 1 : 2,
                      color: state.isConfirmPasswordValid
                          ? Colors.black
                          : Colors.red,
                    ),
                    borderRadius: BorderRadius.circular(4.0),
                  ),
                  child: TextFormField(
                    controller: _confirmPasswordController,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      icon: Icon(Icons.lock),
                      hintText: 'La misma contraseña que definiste arriba',
                      labelText: 'Confirmar Contraseña',
                    ),
                    obscureText: true,
                    autocorrect: false,
                    validator: (_) {
                      return _confirmPasswordController.text ==
                              _passwordController.text
                          ? null
                          : 'Las contraseñas no coinciden';
                    },
                  ),
                ),

                //
                SizedBox(
                  height: 20,
                ),
                RegisterButton(
                  onPressed:
                      isRegisterButtonEnabled(state) ? _onFormSubmitted : null,
                )
              ],
            ),
          ),
        );
      },
    ));
  }

  Widget _campo({
    @required bool validador,
    @required String label,
    @required String hint,
    @required IconData icono,
    @required TextEditingController campoController,
    @required String mensajeError,
    TextInputType keyboardType,
    bool obscureText = false,
  }) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            width: validador ? 1 : 2,
            color: validador ? Colors.black : Colors.red,
          ),
          borderRadius: BorderRadius.circular(4.0),
        ),
        child: TextFormField(
          controller: campoController,
          decoration: InputDecoration(
            icon: Icon(icono),
            border: InputBorder.none,
            hintText: hint,
            labelText: label,
          ),
          keyboardType: keyboardType,
          autocorrect: false,
          obscureText: obscureText,
          validator: (_) {
            return !validador ? mensajeError : null;
          },
        ),
      ),
    );
  }

  void _onEmailChanged() {
    _registerBloc.add(EmailChanged(email: _emailController.text));
  }

  void _onPasswordChanged() {
    _registerBloc.add(PasswordChanged(password: _passwordController.text));
  }

  void _onDNIChanged() {
    _registerBloc.add(DNIChanged(dni: _dniController.text));
  }

  void _onDireccionChanged() {
    _registerBloc.add(DireccionChanged(direccion: _direccionController.text));
  }

  void _onNameChanged() {
    _registerBloc.add(NameChanged(name: _nameController.text));
  }

  void _onApellidoChanged() {
    _registerBloc.add(ApellidoChanged(apellido: _apellidoController.text));
  }

  void _onPhoneChanged() {
    _registerBloc.add(PhoneChanged(phone: _phoneController.text));
  }

  void _onConfirmPasswordChanged() {
    _registerBloc.add(ConfirmPasswordChanged(
        confirmPassword: _confirmPasswordController.text));
  }

  void _onFormSubmitted() {
    _registerBloc.add(
      Submitted(
        email: _emailController.text,
        password: _passwordController.text,
        direccion: _direccionController.text,
        dni: _dniController.text,
        name: _nameController.text,
        apellido: _apellidoController.text,
        phone: _phoneController.text,
      ),
    );
  }
}
