import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:foals/src/services/theme/theme_changer.dart';
import 'package:foals/src/models/producto.dart';
import 'package:foals/src/ui/screens/home/productos/product_card.dart';
import 'package:foals/src/ui/screens/home/widgets/drawer/foals_drawer.dart';
import 'package:foals/src/utils/logo.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bool isDarkModeOn = Provider.of<ThemeChanger>(context).darkMode;
    return Scaffold(
      drawer: FoalsDrawer(),
      appBar: AppBar(
        title: FoalsLogo(height: 35),
        backgroundColor: isDarkModeOn ? Colors.black : Colors.transparent,
        elevation: 0,
        centerTitle: true,
      ),
      body: ListView.builder(
        physics: BouncingScrollPhysics(),
        itemCount: prods.length,
        itemBuilder: (ctx, i) => ProductCard(
          prod: prods[i],
        ),
      ),
    );
  }
}
