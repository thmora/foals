import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:foals/src/models/producto.dart';
import 'package:foals/src/ui/widgets/calavera.dart';

class ProductCard extends StatelessWidget {
  ProductCard({@required this.prod});
  final Producto prod;
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: FlatButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        onPressed: () {},
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 32.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(16.0),
                child: CachedNetworkImage(
                  height: size.height * .5,
                  fit: BoxFit.contain,
                  imageUrl: prod.imagen,
                  placeholder: (_, url) => Center(
                    child: Calavera(),
                  ),
                  errorWidget: (_, url, o) => Center(
                    child: Text('No pudimos cargar la imagen'),
                  ),
                  fadeInDuration: Duration(milliseconds: 350),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Text(prod.nombre, style: TextStyle(fontSize: 20)),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Text("\$${prod.precio}"),
            ),
          ],
        ),
      ),
    );
  }
}

/*  */
