import 'package:flutter/material.dart';
import 'package:foals/src/services/shared_preferences/shared_prefs.dart';

class UserInfo extends StatelessWidget {
  final SharedPrefs prefs = SharedPrefs();
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(top: size.height * .05, left: 16.0),
          child: CircleAvatar(
            radius: size.height * .05,
            backgroundImage: NetworkImage(prefs.photo),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8.0, left: 16.0),
          child: Text(
            "${prefs.nombre}",
            style: TextStyle(fontSize: 20),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8.0, left: 16.0, bottom: 32.0),
          child: Text(
            "${prefs.email}",
            style: TextStyle(fontSize: 15),
          ),
        ),
      ],
    );
  }
}
