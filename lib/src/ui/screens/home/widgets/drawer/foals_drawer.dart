import 'package:flutter/material.dart';
import 'package:foals/src/ui/screens/home/widgets/drawer/user_info.dart';
import 'package:provider/provider.dart';
import 'package:foals/src/utils/styles.dart';
import 'package:foals/src/services/theme/theme_changer.dart';

class FoalsDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChanger>(context);
    return Drawer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          UserInfo(),
          ListTile(
            title: Text("Buzos"),
            trailing: Icon(Icons.accessibility),
            onTap: () {},
          ),
          ListTile(
            title: Text("Remeras"),
            trailing: Icon(Icons.accessibility),
            onTap: () {},
          ),
          ListTile(
            title: Text("Zapatillas"),
            trailing: Icon(Icons.accessibility),
            onTap: () {},
          ),
          Spacer(),
          ListTile(
            title: Text("Nuetras tiendas"),
            trailing: Icon(Icons.location_on, color: Colors.red),
            onTap: () {},
          ),
          ListTile(
            title: Text("Tema Oscuro"),
            trailing: Switch.adaptive(
              activeColor: Colors.white,
              inactiveThumbColor: darkBlack,
              onChanged: (v) => appTheme.darkMode = v,
              value: appTheme.darkMode,
            ),
          ),
        ],
      ),
    );
  }
}
