import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:foals/src/utils/styles.dart';
import 'package:foals/src/services/theme/theme_changer.dart';

class DrawerBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChanger>(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ListTile(
          title: Text("Nuetras tiendas"),
          trailing: Icon(Icons.location_on, color: Colors.red),
          onTap: () {},
        ),
        Spacer(),
        ListTile(
          title: Text("Nuetras tiendas"),
          trailing: Icon(Icons.location_on, color: Colors.red),
          onTap: () {},
        ),
        ListTile(
          title: Text("Tema Oscuro"),
          trailing: Switch.adaptive(
            activeColor: Colors.white,
            inactiveThumbColor: darkBlack,
            onChanged: (v) => appTheme.darkMode = v,
            value: appTheme.darkMode,
          ),
        ),
      ],
    );
  }
}
