import 'package:flutter/material.dart';
import 'package:foals/src/bloc/authentication_bloc/auth_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foals/src/utils/styles.dart';
class LogOutButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 40,
      height: 40,
      child: Center(
        child: IconButton(
          color: colores.darkFAB,
          icon:
              Icon(Icons.arrow_back_ios, color: Colors.black.withOpacity(0.4)),
          onPressed: () {
            BlocProvider.of<AuthBloc>(context).add(LoggedOut());
          },
        ),
      ),
      decoration: BoxDecoration(
        color: colores.darkLetra,
        shape: BoxShape.circle,
        boxShadow: [
          BoxShadow(
            color: Colors.grey[700],
            offset: Offset(4.0, 4.0),
            blurRadius: 10.0,
            spreadRadius: 1.0,
          ),
          BoxShadow(
            color: Colors.grey[300],
            offset: Offset(-4.0, -4.0),
            blurRadius: 10.0,
            spreadRadius: 1.0,
          ),
        ],
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Colors.grey[300],
            Colors.grey[400],
            Colors.grey[500],
            Colors.grey[600],
          ],
        ),
      ),
    );
  }
}

/* onPressed: () {
            BlocProvider.of<AuthBloc>(context).add(LoggedOut());
          }, */
