import 'package:foals/src/ui/widgets/animaciones.dart';
import 'package:foals/src/utils/styles.dart';
import 'package:flutter/material.dart';

class VerAnimacionesButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 150.0, vertical: 20),
      child: Container(
        height: 35,
        child: Center(
          child: FlatButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            highlightColor: colores.darkFlatButtonPressed,
            child:
                Text('Ver Animaciones', style: TextStyle(color: Colors.black)),
            onPressed: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => AnimacionesScreen()));
            },
          ),
        ),
        decoration: BoxDecoration(
          color: colores.darkLetra,
          borderRadius: BorderRadius.circular(8.0),
          boxShadow: [
            BoxShadow(
              color: Colors.grey[700],
              offset: Offset(4.0, 4.0),
              blurRadius: 10.0,
              spreadRadius: 1.0,
            ),
            BoxShadow(
              color: Colors.grey[300],
              offset: Offset(-4.0, -4.0),
              blurRadius: 10.0,
              spreadRadius: 1.0,
            ),
          ],
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Colors.grey[300],
              Colors.grey[400],
              Colors.grey[500],
              Colors.grey[600],
            ],
          ),
        ),
      ),
    );
  }
}
