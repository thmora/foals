import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foals/src/bloc/authentication_bloc/auth_bloc.dart';
import 'package:foals/src/bloc/login_bloc/login_bloc.dart';
import 'package:foals/src/services/repository/user_repository.dart';
import 'package:foals/src/ui/screens/login/widgets/create_account_button.dart';
import 'package:foals/src/ui/screens/login/widgets/google_login_button.dart';
import 'package:foals/src/ui/screens/login/widgets/login_button.dart';
import 'package:foals/src/utils/logo.dart';

class LoginForm extends StatefulWidget {
  LoginForm({Key key, UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);
  final UserRepository _userRepository;
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  UserRepository get _userRepository => widget._userRepository;

  LoginBloc _loginBloc;

  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  bool isLoginButtonEnabled(LoginState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
  }

  void _onEmailChanged() {
    _loginBloc.add(EmailChanged(email: _emailController.text));
  }

  void _onPasswordChanged() {
    _loginBloc.add(PasswordChanged(password: _passwordController.text));
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void _onFormSubmitted() {
    _loginBloc.add(LoginWithCredentialsPressed(
        email: _emailController.text, password: _passwordController.text));
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        // 3 casos
        if (state.isFailure) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Text('Falló al iniciar sesión'),
                backgroundColor: Colors.red,
              ),
            );
        }

        if (state.isSubmitting) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Iniciando sesión...'),
                    CircularProgressIndicator()
                  ],
                ),
                backgroundColor: Theme.of(context).primaryColor,
              ),
            );
        }

        if (state.isSuccess) {
          BlocProvider.of<AuthBloc>(context).add(LoggedIn());
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(
        builder: (context, state) {
          return Padding(
            padding: EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: FoalsLogo(height: 80),
                ),
                Form(
                  child: Column(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: state.isEmailValid ? 1 : 2,
                            color:
                                state.isEmailValid ? Colors.grey : Colors.red,
                          ),
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                        child: TextFormField(
                          controller: _emailController,
                          decoration: InputDecoration(
                            icon: Icon(Icons.mail_outline),
                            border: InputBorder.none,
                            labelText: 'Correo',
                          ),
                          keyboardType: TextInputType.emailAddress,
                          autocorrect: false,
                          validator: (_) {
                            return !state.isEmailValid
                                ? 'Correo invalido'
                                : null;
                          },
                        ),
                      ),
                      SizedBox(height: 20),
                      Container(
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: state.isPasswordValid ? 1 : 2,
                            color: state.isPasswordValid
                                ? Colors.grey
                                : Colors.red,
                          ),
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                        child: TextFormField(
                          controller: _passwordController,
                          decoration: InputDecoration(
                            icon: Icon(Icons.lock_outline),
                            border: InputBorder.none,
                            labelText: 'Contraseña',
                          ),
                          keyboardType: TextInputType.text,
                          autocorrect: false,
                          validator: (_) {
                            return !state.isPasswordValid
                                ? 'Contraseña invalida'
                                : null;
                          },
                          obscureText: true,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            LoginButton(
                              onPressed: isLoginButtonEnabled(state)
                                  ? _onFormSubmitted
                                  : null,
                            ),
                            CreateAccountButton(
                                userRepository: _userRepository),
                            GoogleLoginButton(),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
