import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foals/src/services/repository/user_repository.dart';
import 'package:flutter/material.dart';
import 'package:foals/src/ui/screens/home/home_screen.dart';
import 'package:foals/src/ui/widgets/calavera.dart';

class UserExists extends StatelessWidget {
  UserExists({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);
  final UserRepository _userRepository;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: FirebaseFirestore.instance
          .collection('users')
          .where("uid", isEqualTo: _userRepository.uid)
          .snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Error: ${snapshot.error}');
        }
        if (snapshot.hasData) {
          if (snapshot.connectionState != ConnectionState.none) {
            if (snapshot.data.docs.isNotEmpty) {
              _userRepository
                  .updateSharedPrefsUserData(snapshot.data.docs.first.id);
            } else {
              _userRepository
                  .updateSharedPrefsUserData(_userRepository.signUpUserInDB());
            }
            return HomeScreen();
          } else {
            return Scaffold(
              body: Center(
                child: Text('No hay conexión a internet'),
              ),
            );
          }
        }
        return Scaffold(body: Center(child: Calavera()));
      },
    );
  }
}
