
import 'package:foals/src/services/repository/user_repository.dart';
import 'package:foals/src/ui/screens/register/register_screen.dart';
import 'package:foals/src/utils/styles.dart';
import 'package:flutter/material.dart';

class CreateAccountButton extends StatelessWidget {
  CreateAccountButton({Key key, @required UserRepository userRepository})
      : _userRepository = userRepository,
        super(key: key);

  final UserRepository _userRepository;
  @override
  Widget build(BuildContext context) {
    return FlatButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      onPressed: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (_) => RegisterScreen(
              userRepository: _userRepository,
            ),
          ),
        );
      },
      child: Text(
        '¿No tienes una cuenta? Registrate aquí.',
        style: TextStyle(fontSize: 14, color: colores.darkLetra),
      ),
    );
  }
}
