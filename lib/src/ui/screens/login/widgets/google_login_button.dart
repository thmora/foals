import 'package:foals/src/utils/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foals/src/bloc/login_bloc/login_bloc.dart';

class GoogleLoginButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GoogleSignInButton(
      text: 'Iniciar Sesión con Google',
      darkMode: true,
      borderRadius: 8.0,
      onPressed: () {
        Scaffold.of(context).showSnackBar(
          SnackBar(
            content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Iniciando Sesión con Google..'),
                CircularProgressIndicator()
              ],
            ),
            backgroundColor: colores.covidBlue,
          ),
        );
        BlocProvider.of<LoginBloc>(context).add(LoginWithGooglePressed());
      },
    );
  }
}
