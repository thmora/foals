import 'package:foals/src/bloc/authentication_bloc/auth_bloc.dart';
import 'package:foals/src/services/repository/user_repository.dart';
import 'package:foals/src/ui/screens/login/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foals/src/services/theme/theme_changer.dart';
import 'package:foals/src/ui/screens/login/widgets/user_exists.dart';
import 'package:provider/provider.dart';
import 'package:foals/src/ui/screens/splash_screen.dart';

class FoalsApp extends StatelessWidget {
  final UserRepository _userRepository;

  FoalsApp({Key key, UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);
  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChanger>(context).currentTheme;
    return MaterialApp(
      title: 'FOALS',
      home: BlocBuilder<AuthBloc, AuthState>(
        builder: (context, state) {
          if (state is NoInicializado) {
            return SplashScreen();
          }
          if (state is Autenticado) {
            return UserExists(userRepository: _userRepository);
          }
          if (state is NoAutenticado) {
            return LoginScreen(userRepository: _userRepository);
          }
          return Container();
        },
      ),
      theme: appTheme,
    );
  }
}
