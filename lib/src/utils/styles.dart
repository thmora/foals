import 'package:flutter/material.dart';

final colores = _Colores();

class _Colores {
  final Color covidBlack = Color(0xff262d2f);
  final Color covidBlue = Color(0xFF418dff);
  final Color covidGrey = Color(0xffbdbdbd);
  final Color covidLightblue = Color(0xFF3ccfe6);
  final Color covidWhite = Color(0xFFdae3f2);
  final Color darkFAB = Color(0xFF80cbc4);
  final Color darkFlatButtonPressed = Color(0xFF4f4f4f);
  final Color darkFondo = Color(0xFF303030);
  final Color darkLetra = Color(0xFF95989a);
  final Color deepOceanGreen = Color(0xFF48817c);
  final Color rosaPastel = Color(0xFFf6aaad);
  final Color cobaltBlue = Color(0xFF007bfa);
  final Color black21 = Color(0xff212121);
  final Color greyE1 = Color(0xFFE1E1E1);
  final Color violeta = Color(0xFF73237f);
  final Color black30 = Color(0xFF303030);
}

MaterialColor darkBlack = MaterialColor(
  0xFF303030,
  {
    100: Color(0xFF303030).withOpacity(0.1),
    200: Color(0xFF303030).withOpacity(0.2),
    300: Color(0xFF303030).withOpacity(0.3),
    400: Color(0xFF303030).withOpacity(0.4),
    500: Color(0xFF303030).withOpacity(0.5),
    600: Color(0xFF303030).withOpacity(0.6),
    700: Color(0xFF303030).withOpacity(0.7),
    800: Color(0xFF303030).withOpacity(0.8),
    900: Color(0xFF303030).withOpacity(0.9),
  },
);
