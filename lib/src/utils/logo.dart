import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:foals/src/services/theme/theme_changer.dart';

class FoalsLogo extends StatelessWidget {
  FoalsLogo({this.height = 50});
  final double height;
  @override
  Widget build(BuildContext context) {
    final bool isDarkModeOn = Provider.of<ThemeChanger>(context).darkMode;
    return Image.asset(
      isDarkModeOn ? 'assets/foals_white.png' : 'assets/foals_black.png',
      height: height,
    );
  }
}
