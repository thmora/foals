part of 'register_bloc.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();
  @override
  List<Object> get props => [];
}

class EmailChanged extends RegisterEvent {
  final String email;

  const EmailChanged({@required this.email});

  @override
  List<Object> get props => [email];

  @override
  String toString() => 'EmailChanged: {email: $email}';
}

class PasswordChanged extends RegisterEvent {
  final String password;
  const PasswordChanged({@required this.password});
  @override
  List<Object> get props => [password];
  @override
  String toString() => 'PasswordChanged: {password: $password}';
}

class DireccionChanged extends RegisterEvent {
  final String direccion;
  const DireccionChanged({@required this.direccion});
  @override
  List<Object> get props => [direccion];
  @override
  String toString() => 'DireccionChanged: {direccion: $direccion}';
}

class DNIChanged extends RegisterEvent {
  final String dni;
  const DNIChanged({@required this.dni});
  @override
  List<Object> get props => [dni];
  @override
  String toString() => 'DNIChanged: {DNI: $dni}';
}

class NameChanged extends RegisterEvent {
  final String name;
  const NameChanged({@required this.name});
  @override
  List<Object> get props => [name];
  @override
  String toString() => 'NameChanged: {Name: $name}';
}

class ApellidoChanged extends RegisterEvent {
  final String apellido;
  const ApellidoChanged({@required this.apellido});
  @override
  List<Object> get props => [apellido];
  @override
  String toString() => 'ApellidoChanged: {Apellido: $apellido}';
}

class ConfirmPasswordChanged extends RegisterEvent {
  final String confirmPassword;
  const ConfirmPasswordChanged({@required this.confirmPassword});
  @override
  List<Object> get props => [confirmPassword];
  @override
  String toString() =>
      'ConfirmPasswordChanged: {ConfirmPassword: $confirmPassword}';
}

class PhoneChanged extends RegisterEvent {
  final String phone;
  const PhoneChanged({@required this.phone});
  @override
  List<Object> get props => [phone];
  @override
  String toString() => 'PhoneChanged: {Phone: $phone}';
}

class Submitted extends RegisterEvent {
  final String password;
  final String email;
  final String direccion;
  final String phone;
  final String apellido;
  final String dni;
  final String name;

  const Submitted({
    @required this.password,
    @required this.direccion,
    @required this.email,
    @required this.phone,
    @required this.name,
    @required this.apellido,
    @required this.dni,
  });

  @override
  List<Object> get props =>
      [email, password, direccion, dni, name, apellido, phone];

  @override
  String toString() =>
      'Register Submitted: {email: $email, password: $password, direccion: $direccion, dni: $dni,  name: $name, apellido: $apellido, phone: $phone,}';
}
