part of 'register_bloc.dart';

class RegisterState {
  final bool isEmailValid;
  final bool isPasswordValid;
  final bool isDireccionValid;
  final bool isDNIValid;
  final bool isNameValid;
  final bool isApellidoValid;
  final bool isPhoneValid;
  final bool isConfirmPasswordValid;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;

  RegisterState({
    @required this.isEmailValid,
    @required this.isPasswordValid,
    @required this.isDireccionValid,
    @required this.isDNIValid,
    @required this.isNameValid,
    @required this.isApellidoValid,
    @required this.isPhoneValid,
    @required this.isConfirmPasswordValid,
    @required this.isSubmitting,
    @required this.isSuccess,
    @required this.isFailure,
  });

  bool get isFormValid => isEmailValid && isPasswordValid;

  factory RegisterState.empty() {
    return RegisterState(
      isEmailValid: true,
      isPasswordValid: true,
      isDNIValid: true,
      isDireccionValid: true,
      isNameValid: true,
      isApellidoValid: true,
      isPhoneValid: true,
      isConfirmPasswordValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory RegisterState.loading() {
    return RegisterState(
      isEmailValid: true,
      isPasswordValid: true,
      isDNIValid: true,
      isDireccionValid: true,
      isNameValid: true,
      isApellidoValid: true,
      isPhoneValid: true,
      isConfirmPasswordValid: true,
      isSubmitting: true,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory RegisterState.failure() {
    return RegisterState(
      isEmailValid: true,
      isPasswordValid: true,
      isDireccionValid: true,
      isDNIValid: true,
      isNameValid: true,
      isApellidoValid: true,
      isPhoneValid: true,
      isConfirmPasswordValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: true,
    );
  }

  factory RegisterState.success() {
    return RegisterState(
      isEmailValid: true,
      isPasswordValid: true,
      isDireccionValid: true,
      isDNIValid: true,
      isNameValid: true,
      isApellidoValid: true,
      isPhoneValid: true,
      isConfirmPasswordValid: true,
      isSubmitting: false,
      isSuccess: true,
      isFailure: false,
    );
  }

  RegisterState copyWith({
    bool isEmailValid,
    bool isPasswordValid,
    bool isDNIValid,
    bool isDireccionValid,
    bool isNameValid,
    bool isApellidoValid,
    bool isPhoneValid,
    bool isConfirmPasswordValid,
    bool isSubmitting,
    bool isSuccess,
    bool isFailure,
  }) {
    return RegisterState(
      isEmailValid: isEmailValid ?? this.isEmailValid,
      isPasswordValid: isPasswordValid ?? this.isPasswordValid,
      isDNIValid: isDNIValid ?? this.isDNIValid,
      isDireccionValid: isDireccionValid ?? this.isDireccionValid,
      isNameValid: isNameValid ?? this.isNameValid,
      isApellidoValid: isApellidoValid ?? this.isApellidoValid,
      isPhoneValid: isPhoneValid ?? this.isPhoneValid,
      isConfirmPasswordValid:
          isConfirmPasswordValid ?? this.isConfirmPasswordValid,
      isSubmitting: isSubmitting ?? this.isSubmitting,
      isSuccess: isSuccess ?? this.isSuccess,
      isFailure: isFailure ?? this.isFailure,
    );
  }

  RegisterState update({
    bool isEmailValid,
    bool isPasswordValid,
    bool isDNIValid,
    bool isDireccionValid,
    bool isNameValid,
    bool isApellidoValid,
    bool isPhoneValid,
    bool isConfirmPasswordValid,
  }) {
    return copyWith(
      isEmailValid: isEmailValid,
      isPasswordValid: isPasswordValid,
      isDireccionValid: isDireccionValid,
      isDNIValid: isDNIValid,
      isNameValid: isNameValid,
      isApellidoValid: isApellidoValid,
      isPhoneValid: isPhoneValid,
      isConfirmPasswordValid: isConfirmPasswordValid,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  @override
  String toString() {
    return '''
    RegisterState(
      isEmailValid: $isEmailValid,
      isPasswordValid: $isPasswordValid,
      y  demas campos
      isSubmitting: $isSubmitting,
      isSuccess: $isSuccess,
      isFailure: $isFailure,
    )
    ''';
  }
}
