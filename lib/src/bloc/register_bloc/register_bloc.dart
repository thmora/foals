import 'package:flutter/material.dart';
import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:foals/src/services/repository/user_repository.dart';
import 'package:foals/src/services/validators.dart';

part 'register_event.dart';
part 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final UserRepository _userRepository;

  RegisterBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(RegisterState.empty());

  /*  @override
  Stream<RegisterState> transformEvents(Stream<RegisterEvent> events,
      Stream<RegisterState> Function(RegisterEvent) next) {
    final nonDebounceStream = events.where((event) {
      return (event is! EmailChanged && event is! PasswordChanged);
    });

    final debounceStream = events.where((event) {
      return (event is EmailChanged || event is PasswordChanged);
    }).debounceTime(Duration(milliseconds: 300));

    return super
        .transformEvents(nonDebounceStream.mergeWith([debounceStream]), next);
  } */

  @override
  Stream<RegisterState> mapEventToState(
    RegisterEvent event,
  ) async* {
    if (event is EmailChanged) {
      yield* _mapEmailChangedToState(event.email);
    }
    if (event is PasswordChanged) {
      yield* _mapPasswordChangedToState(event.password);
    }
    if (event is DireccionChanged) {
      yield* _mapDireccionChangedToState(event.direccion);
    }
    if (event is DNIChanged) {
      yield* _mapDNIChangedToState(event.dni);
    }
    if (event is NameChanged) {
      yield* _mapNameChangedToState(event.name);
    }
    if (event is ApellidoChanged) {
      yield* _mapApellidoChangedToState(event.apellido);
    }
    if (event is PhoneChanged) {
      yield* _mapPhoneChangedToState(event.phone);
    }
    if (event is ConfirmPasswordChanged) {
      yield* _mapConfirmPasswordChangedToState(event.confirmPassword);
    }
    if (event is Submitted) {
      yield* _mapFormSubmittedToState(
        email: event.email,
        password: event.password,
        direccion: event.direccion,
        dni: event.dni,
        name: event.name,
        apellido: event.apellido,
        phone: event.phone,
      );
    }
  }

  Stream<RegisterState> _mapEmailChangedToState(String email) async* {
    yield state.update(isEmailValid: Validators.isValidEmail(email));
  }

  Stream<RegisterState> _mapPasswordChangedToState(String password) async* {
    yield state.update(isPasswordValid: Validators.isValidPassword(password));
  }

  Stream<RegisterState> _mapDireccionChangedToState(String direccion) async* {
    yield state.update(
        isDireccionValid: Validators.isValidDireccion(direccion));
  }

  Stream<RegisterState> _mapNameChangedToState(String name) async* {
    yield state.update(isNameValid: Validators.isValidName(name));
  }

  Stream<RegisterState> _mapApellidoChangedToState(String apellido) async* {
    yield state.update(isApellidoValid: Validators.isValidApellido(apellido));
  }

  Stream<RegisterState> _mapDNIChangedToState(String dni) async* {
    yield state.update(isDNIValid: Validators.isValidDNI(dni));
  }

  Stream<RegisterState> _mapPhoneChangedToState(String phone) async* {
    yield state.update(isPhoneValid: Validators.isValidPhone(phone));
  }

  Stream<RegisterState> _mapConfirmPasswordChangedToState(
      String confirmPassword) async* {
    yield state.update(
        isConfirmPasswordValid:
            Validators.isValidConfirmPassword(confirmPassword));
  }

  Stream<RegisterState> _mapFormSubmittedToState({
    String email,
    String password,
    String direccion,
    String dni,
    String name,
    String apellido,
    String phone,
  }) async* {
    yield RegisterState.loading();
    try {
      await _userRepository.signUp(
        email: email,
        password: password,
        direccion: direccion,
        dni: dni,
        name: name,
        apellido: apellido,
        phone: phone,
      );
      yield RegisterState.success();
    } catch (_) {
      yield RegisterState.failure();
    }
  }
}
