part of 'login_bloc.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();

  @override
  List<Object> get props => [];
}
//cinco eventos

// Email Changed - notifica al BLoC que el usuario cambió de email

class EmailChanged extends LoginEvent {
  final String email;

  const EmailChanged({@required this.email});

  @override
  List<Object> get props => [email];

  @override
  String toString() => 'EmailChanged: {email: $email}';
}

// Password Changed - notifica al BLoC que el usuario cambió de contraseña

class PasswordChanged extends LoginEvent {
  final String password;

  const PasswordChanged({@required this.password});

  @override
  List<Object> get props => [password];

  @override
  String toString() => 'PasswordChanged: {password: $password}';
}

// Submitted - notifica al BLoC que el usuario cambió la informacion del formulario

class Submitted extends LoginEvent {
  final String password;
  final String email;

  const Submitted({@required this.password, @required this.email});

  @override
  List<Object> get props => [email, password];

  @override
  String toString() => 'Submitted: {email: $email, password: $password}';
}

// login with google
class LoginWithGooglePressed extends LoginEvent {}
// LoginWithCredentialsPressed - notifica al BLoC que el usuario inició sesión de forma convencional (email y password)

class LoginWithCredentialsPressed extends LoginEvent {
  final String password;
  final String email;

  const LoginWithCredentialsPressed(
      {@required this.password, @required this.email});

  @override
  List<Object> get props => [email, password];

  @override
  String toString() =>
      'LoginWithCredentialsPressed: {email: $email, password: $password}';
}
