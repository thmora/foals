import 'package:flutter/cupertino.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:foals/src/services/repository/user_repository.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final UserRepository _userRepository;

  AuthBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(NoAutenticado());

  @override
  Stream<AuthState> mapEventToState(
    AuthEvent event,
  ) async* {
    if (event is AppStarted) {
      yield* _mapAppStatedToState();
    }
    if (event is LoggedIn) {
      yield* _mapLoggedInToState();
    }
    if (event is LoggedOut) {
      yield* _mapLoggedOutToState();
    }
  }

  Stream<AuthState> _mapAppStatedToState() async* {
    try {
      final isSignedIn = await _userRepository.isSignedIn();
      if (isSignedIn) {
        yield Autenticado(
          _userRepository.email,
          _userRepository.name,
          _userRepository.photo,
          _userRepository.uid,
        );
      } else {
        NoAutenticado();
      }
    } catch (_) {
      yield NoAutenticado();
    }
  }

  Stream<AuthState> _mapLoggedInToState() async* {
    yield Autenticado(
      _userRepository.email,
      _userRepository.name,
      _userRepository.photo,
      _userRepository.uid,
    );
  }

  Stream<AuthState> _mapLoggedOutToState() async* {
    yield NoAutenticado();
    _userRepository.signOut();
  }
}
