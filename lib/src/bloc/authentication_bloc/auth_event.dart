part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => [];
}

class AppStarted extends AuthEvent {
  @override
  String toString() {
    return 'evento de app iniciada';
  }
}

class LoggedIn extends AuthEvent {
  @override
  String toString() {
    return 'evento de log in';
  }
}

class LoggedOut extends AuthEvent {
  @override
  String toString() {
    return 'evento de log out';
  }
}
