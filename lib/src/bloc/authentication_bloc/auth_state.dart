part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];
}

class NoInicializado extends AuthState {
  @override
  String toString() => 'No Inicializado';
}

class Autenticado extends AuthState {
  final String email;
  final String name;
  final String photo;
  final String uid;

  const Autenticado(this.email, this.name, this.photo, this.uid);

  @override
  List<Object> get props => [email];

  @override
  String toString() => 'Autenticado - $email, $name, $uid, $photo';
}

class NoAutenticado extends AuthState {
  @override
  String toString() => 'No Autenticado';
}
