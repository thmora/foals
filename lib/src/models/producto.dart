class Producto {
  final String nombre;
  final double precio;
  final String imagen;
  Producto({
    this.nombre,
    this.precio,
    this.imagen,
  });
}

final List<Producto> prods = [
  Producto(
    imagen:
        "https://scontent.fnqn2-1.fna.fbcdn.net/v/t1.0-9/120844823_2729867507287816_9130104075734570790_o.jpg?_nc_cat=107&_nc_sid=a26aad&_nc_eui2=AeGbxiZ0pPi2tmpGpHT8Amq8Nv-Svj0cFCE2_5K-PRwUIY-H7O1dAHtlQcwq2r7jWGf-tWiGq7PkYOpk6_svEaVp&_nc_ohc=v-anS6h4lxUAX_CgVAp&_nc_ht=scontent.fnqn2-1.fna&oh=58da2c6d132b78b7f72b1dfac05e6141&oe=5FA498AB",
    nombre: "Buzo Fino Combiando #GRIX",
    precio: 2400,
  ),
  Producto(
    imagen: "https://scontent.fnqn2-1.fna.fbcdn.net/v/t1.0-9/120902452_2728097744131459_1647277215292495523_o.jpg?_nc_cat=111&_nc_sid=a26aad&_nc_eui2=AeE6gCEKoVCfBslYr4WoASIvVEHiiZl9vNVUQeKJmX281TcCszmg-5f0LZCuswM0dj1gug_1w9YQP_bbaWodU96o&_nc_ohc=ej6Bjh8FggMAX8seMIJ&_nc_ht=scontent.fnqn2-1.fna&oh=d59a3892659a067e1505a15bc5c1a836&oe=5FA464EA",
    nombre: "Camisa #Leñadora #RED ",
    precio: 2600,
  ),
  Producto(
    imagen:
        "https://scontent.fnqn2-2.fna.fbcdn.net/v/t1.0-9/120344773_2723650747909492_4191785873477016403_o.jpg?_nc_cat=110&_nc_sid=a26aad&_nc_eui2=AeEe443t2YDGjffRqfxK04Z3Ibu3GJ2aI74hu7cYnZojvnAp9BnGitP7fSIrMs1iUbSm83D3Afw9maXPbjQKxMD5&_nc_ohc=xnBOvAg3aJIAX8WnZT5&_nc_ht=scontent.fnqn2-2.fna&oh=f605b1c1f14323bf002d2006c8a1577a&oe=5FA61593",
    nombre: "Remera #Bordo",
    precio: 2400,
  ),
];
