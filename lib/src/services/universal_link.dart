import 'package:url_launcher/url_launcher.dart';

universalLink(
  String url, [
  bool esReserva = false,
  String telefono = '',
]) async {
  if (esReserva) {
    if (url.isEmpty) {
      url = 'sms:$telefono';
    }
  }
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'numero está mal ($url), tiene que tener formato +PAIS AREA y NUMERO';
  }
}

universalLinkInfo(String url) async {
  if (url.contains('+')) {
    url = 'sms:$url';
  }
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'numero está mal ($url), tiene que tener formato +PAIS AREA y NUMERO';
  }
}
