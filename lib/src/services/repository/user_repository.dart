import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:foals/src/services/shared_preferences/shared_prefs.dart';

class UserRepository {
  final FirebaseAuth _firebaseAuth;
  final GoogleSignIn _googleSignIn;

  UserRepository({
    FirebaseAuth firebaseAuth,
    GoogleSignIn googleSignIn,
  })  : _firebaseAuth = firebaseAuth ?? FirebaseAuth.instance,
        _googleSignIn = googleSignIn ?? GoogleSignIn();

  Future<void> signInWithCredentials(String email, String password) {
    return _firebaseAuth.signInWithEmailAndPassword(
      email: email,
      password: password,
    );
  }

  Future<dynamic> signInWithGoogle() async {
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;
    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    await _firebaseAuth.signInWithCredential(credential);
    return _firebaseAuth.currentUser;
  }

  Future<void> signUp({
    String email,
    String password,
    String direccion,
    String dni,
    String name,
    String apellido,
    String phone,
  }) async {
    FirebaseFirestore.instance.collection('users').add({
      'email': email,
      'password': password,
      'direccion': direccion,
      'dni': dni,
      'name': name,
      'apellido': apellido,
      'phone': phone,
    });
    return await _firebaseAuth.createUserWithEmailAndPassword(
      email: email,
      password: password,
    );
  }

  Future<void> signOut() async {
    await _firebaseAuth.signOut();
    await _googleSignIn.signOut();
  }

  Future<bool> isSignedIn() async {
    final currentUser = _firebaseAuth.currentUser;
    return currentUser != null;
  }

  String get email => _firebaseAuth.currentUser.email;

  String get name => _firebaseAuth.currentUser.displayName;

  String get photo => _firebaseAuth.currentUser.photoURL;

  String get uid => _firebaseAuth.currentUser.uid;

  String signUpUserInDB() {
    final DocumentReference docRef =
        FirebaseFirestore.instance.collection('usuarios').doc();
    docRef.set({
      'uid': _firebaseAuth.currentUser.uid,
      'nombre': _firebaseAuth.currentUser.displayName,
      'email': _firebaseAuth.currentUser.email,
      'photoUrl': _firebaseAuth.currentUser.photoURL,
    });
    return docRef.id;
  }

  void updateSharedPrefsUserData(String docID) {
    final SharedPrefs _prefs = SharedPrefs();
    _prefs.docID = docID;
    _prefs.uid = _firebaseAuth.currentUser.uid;
    _prefs.nombre = _firebaseAuth.currentUser.displayName;
    _prefs.photo = _firebaseAuth.currentUser.photoURL;
    _prefs.email = _firebaseAuth.currentUser.email;
  }
}
