import 'package:foals/src/services/shared_preferences/shared_prefs.dart';
import 'package:foals/src/utils/styles.dart';
import 'package:flutter/material.dart';

final ThemeData _lightTheme = ThemeData(
  primarySwatch: darkBlack,
  primaryColor: darkBlack,
  secondaryHeaderColor: Colors.red,
  bottomSheetTheme: BottomSheetThemeData(backgroundColor: Colors.white),
  snackBarTheme: SnackBarThemeData(
    contentTextStyle: TextStyle(
      color: Colors.white,
    ),
  ),
  appBarTheme: AppBarTheme(iconTheme: IconThemeData(color: Colors.black)),
  accentColor: Colors.purple,
);

final ThemeData _darkTheme = ThemeData(
  primarySwatch: darkBlack,
  primaryColor: darkBlack,
  brightness: Brightness.dark,
  accentColor: Colors.purple,
  hintColor: Colors.white,
  dialogTheme: DialogTheme(backgroundColor: colores.black30),
  bottomSheetTheme: BottomSheetThemeData(backgroundColor: colores.black30),
  snackBarTheme: SnackBarThemeData(
    contentTextStyle: TextStyle(
      color: Colors.white,
    ),
  ),
  dialogBackgroundColor: colores.black30,
);

final SharedPrefs _prefs = SharedPrefs();

class ThemeChanger with ChangeNotifier {
  bool _darkMode = _prefs.darkMode;
  ThemeData _currentTheme = !_prefs.darkMode ? _lightTheme : _darkTheme;

  bool get darkMode => this._darkMode;
  ThemeData get currentTheme => this._currentTheme;

  set darkMode(bool value) {
    _darkMode = value;
    _prefs.darkMode = value;
    if (value) {
      _currentTheme = _darkTheme;
    } else {
      _currentTheme = _lightTheme;
    }
    notifyListeners();
  }
}
