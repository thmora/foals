import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs {
  static final SharedPrefs _instance = SharedPrefs._internal();

  factory SharedPrefs() {
    return _instance;
  }
  SharedPrefs._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    _prefs = await SharedPreferences.getInstance();
  }

  get notif {
    return _prefs.getBool('notif') ?? true;
  }

  set notif(bool turnNotif) {
    _prefs.setBool('notif', turnNotif);
  }

  get email {
    return _prefs.getString('email') ?? '';
  }

  set email(String value) {
    _prefs.setString('email', value);
  }

  get photo {
    return _prefs.getString('photo') ?? '';
  }

  set photo(String value) {
    _prefs.setString('photo', value);
  }

  get nombre {
    return _prefs.getString('nombre') ?? '';
  }

  set nombre(String value) {
    _prefs.setString('nombre', value);
  }

  get uid {
    return _prefs.getString('uid') ?? '';
  }

  set uid(String value) {
    _prefs.setString('uid', value);
  }

  get docID {
    return _prefs.getString('docID') ?? '';
  }

  set docID(String value) {
    _prefs.setString('docID', value);
  }

  get pag {
    return _prefs.getInt('pag') ?? 0;
  }

  set pag(int value) {
    _prefs.setInt('pag', value);
  }

  get darkMode {
    return _prefs.getBool('darkMode') ?? true;
  }

  set darkMode(bool value) {
    _prefs.setBool('darkMode', value);
  }
}
