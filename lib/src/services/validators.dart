class Validators {
  // 2 funciones

  // crear RegExp

  // email:
  static final RegExp _emailRegExp = RegExp(
    r'^[a-zA-Z0-9.!#$%&’*+&=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$',
  );

  // password:

  static final RegExp _passwordRegExp = RegExp(
    r'^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$',
  );

  // isValidEmail
  static isValidEmail(String email) {
    return _emailRegExp.hasMatch(email);
  }

  static isValidPassword(String password) {
    return _passwordRegExp.hasMatch(password);
  }

  static isValidDNI(String dni) {
    bool valor = false;
    if (dni.isNotEmpty && dni.length == 8 || dni.length == 7) {
      valor = true;
    }
    return valor;
  }

  static isValidName(String name) {
    return name.isNotEmpty;
  }

  static isValidApellido(String apellido) {
    return apellido.isNotEmpty;
  }

  static isValidDireccion(String direccion) {
    bool valor = false;
    if (direccion.isNotEmpty && direccion.length >= 6) {
      valor = true;
    }
    return valor;
  }

  static isValidPhone(String phone){
    return phone.isNotEmpty && phone.length == 10;
  }

  static isValidConfirmPassword(String confirmPassword){
    return confirmPassword.isNotEmpty && confirmPassword == confirmPassword;
  }
}
