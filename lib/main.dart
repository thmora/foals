import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:foals/src/bloc/authentication_bloc/auth_bloc.dart';
import 'package:foals/src/bloc/simple_bloc_observer.dart';
import 'package:foals/src/foals_app.dart';
import 'package:foals/src/services/shared_preferences/shared_prefs.dart';
import 'package:foals/src/services/repository/user_repository.dart';
import 'package:provider/provider.dart';
import 'package:foals/src/services/theme/theme_changer.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Bloc.observer = SimpleBlocObserver();
  await Firebase.initializeApp();
  final UserRepository _userRepository = UserRepository();
  final SharedPrefs _prefs = SharedPrefs();
  await _prefs.initPrefs();
  SystemChrome.setPreferredOrientations(([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]));
  runApp(
    BlocProvider(
      create: (context) =>
          AuthBloc(userRepository: _userRepository)..add(AppStarted()),
      child: ChangeNotifierProvider(
        create: (_) => ThemeChanger(),
        child: FoalsApp(userRepository: _userRepository),
      ),
    ),
  );
}

